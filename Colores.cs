﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procesamiento_de_imagenes
{
    class Colores
    {
        public List<int> r = new List<int>();
        public List<int> g = new List<int>();
        public List<int> b = new List<int>();

        public Colores()
        {
            Random random = new Random();
            for (int n = 0; n < 6; n++)
            {
                r.Add(random.Next(0, 255));
                g.Add(random.Next(0, 255));
                b.Add(random.Next(0, 255));
            }
        }
    }
}
