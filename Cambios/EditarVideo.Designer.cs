﻿
namespace Procesamiento_de_imagenes.Cambios
{
    partial class EditarVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Filtro5 = new System.Windows.Forms.Button();
            this.Filtro4 = new System.Windows.Forms.Button();
            this.Filtro3 = new System.Windows.Forms.Button();
            this.VFiltro2 = new System.Windows.Forms.Button();
            this.VFiltro01 = new System.Windows.Forms.Button();
            this.PB_Video = new System.Windows.Forms.PictureBox();
            this.SeleccVideo = new System.Windows.Forms.Button();
            this.btn_detener = new System.Windows.Forms.Button();
            this.btn_play = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Video)).BeginInit();
            this.SuspendLayout();
            // 
            // Filtro5
            // 
            this.Filtro5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Filtro5.FlatAppearance.BorderSize = 3;
            this.Filtro5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Filtro5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Filtro5.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Filtro5.ForeColor = System.Drawing.Color.White;
            this.Filtro5.Location = new System.Drawing.Point(807, 445);
            this.Filtro5.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.Filtro5.Name = "Filtro5";
            this.Filtro5.Size = new System.Drawing.Size(213, 49);
            this.Filtro5.TabIndex = 15;
            this.Filtro5.Text = "OR";
            this.Filtro5.UseVisualStyleBackColor = true;
            this.Filtro5.Click += new System.EventHandler(this.Filtro5_Click);
            // 
            // Filtro4
            // 
            this.Filtro4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Filtro4.FlatAppearance.BorderSize = 3;
            this.Filtro4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Filtro4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Filtro4.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Filtro4.ForeColor = System.Drawing.Color.White;
            this.Filtro4.Location = new System.Drawing.Point(807, 355);
            this.Filtro4.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.Filtro4.Name = "Filtro4";
            this.Filtro4.Size = new System.Drawing.Size(213, 49);
            this.Filtro4.TabIndex = 14;
            this.Filtro4.Text = "AND";
            this.Filtro4.UseVisualStyleBackColor = true;
            this.Filtro4.Click += new System.EventHandler(this.Filtro4_Click);
            // 
            // Filtro3
            // 
            this.Filtro3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Filtro3.FlatAppearance.BorderSize = 3;
            this.Filtro3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Filtro3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Filtro3.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Filtro3.ForeColor = System.Drawing.Color.White;
            this.Filtro3.Location = new System.Drawing.Point(807, 265);
            this.Filtro3.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.Filtro3.Name = "Filtro3";
            this.Filtro3.Size = new System.Drawing.Size(213, 49);
            this.Filtro3.TabIndex = 13;
            this.Filtro3.Text = "Adicion";
            this.Filtro3.UseVisualStyleBackColor = true;
            this.Filtro3.Click += new System.EventHandler(this.Filtro3_Click);
            // 
            // VFiltro2
            // 
            this.VFiltro2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.VFiltro2.FlatAppearance.BorderSize = 3;
            this.VFiltro2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.VFiltro2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VFiltro2.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VFiltro2.ForeColor = System.Drawing.Color.White;
            this.VFiltro2.Location = new System.Drawing.Point(807, 175);
            this.VFiltro2.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.VFiltro2.Name = "VFiltro2";
            this.VFiltro2.Size = new System.Drawing.Size(213, 49);
            this.VFiltro2.TabIndex = 12;
            this.VFiltro2.Text = "Prewitt";
            this.VFiltro2.UseVisualStyleBackColor = true;
            this.VFiltro2.Click += new System.EventHandler(this.VFiltro2_Click);
            // 
            // VFiltro01
            // 
            this.VFiltro01.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.VFiltro01.FlatAppearance.BorderSize = 3;
            this.VFiltro01.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.VFiltro01.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VFiltro01.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VFiltro01.ForeColor = System.Drawing.Color.White;
            this.VFiltro01.Location = new System.Drawing.Point(807, 85);
            this.VFiltro01.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.VFiltro01.Name = "VFiltro01";
            this.VFiltro01.Size = new System.Drawing.Size(213, 49);
            this.VFiltro01.TabIndex = 11;
            this.VFiltro01.Text = "Laplaciano";
            this.VFiltro01.UseVisualStyleBackColor = true;
            this.VFiltro01.Click += new System.EventHandler(this.VFiltro01_Click);
            // 
            // PB_Video
            // 
            this.PB_Video.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PB_Video.Location = new System.Drawing.Point(172, 172);
            this.PB_Video.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.PB_Video.Name = "PB_Video";
            this.PB_Video.Size = new System.Drawing.Size(540, 353);
            this.PB_Video.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PB_Video.TabIndex = 10;
            this.PB_Video.TabStop = false;
            // 
            // SeleccVideo
            // 
            this.SeleccVideo.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.SeleccVideo.FlatAppearance.BorderSize = 3;
            this.SeleccVideo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.SeleccVideo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SeleccVideo.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SeleccVideo.ForeColor = System.Drawing.Color.White;
            this.SeleccVideo.Location = new System.Drawing.Point(337, 85);
            this.SeleccVideo.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.SeleccVideo.Name = "SeleccVideo";
            this.SeleccVideo.Size = new System.Drawing.Size(213, 49);
            this.SeleccVideo.TabIndex = 9;
            this.SeleccVideo.Text = "Seleccionar Video";
            this.SeleccVideo.UseVisualStyleBackColor = true;
            this.SeleccVideo.Click += new System.EventHandler(this.SeleccVideo_Click);
            // 
            // btn_detener
            // 
            this.btn_detener.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_detener.FlatAppearance.BorderSize = 3;
            this.btn_detener.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_detener.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_detener.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_detener.ForeColor = System.Drawing.Color.White;
            this.btn_detener.Location = new System.Drawing.Point(499, 553);
            this.btn_detener.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_detener.Name = "btn_detener";
            this.btn_detener.Size = new System.Drawing.Size(213, 49);
            this.btn_detener.TabIndex = 16;
            this.btn_detener.Text = "Detener Video";
            this.btn_detener.UseVisualStyleBackColor = true;
            this.btn_detener.Click += new System.EventHandler(this.btn_detener_Click);
            // 
            // btn_play
            // 
            this.btn_play.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_play.FlatAppearance.BorderSize = 3;
            this.btn_play.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_play.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_play.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_play.ForeColor = System.Drawing.Color.White;
            this.btn_play.Location = new System.Drawing.Point(216, 553);
            this.btn_play.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_play.Name = "btn_play";
            this.btn_play.Size = new System.Drawing.Size(213, 49);
            this.btn_play.TabIndex = 17;
            this.btn_play.Text = "Play";
            this.btn_play.UseVisualStyleBackColor = true;
            this.btn_play.Click += new System.EventHandler(this.btn_play_Click);
            // 
            // EditarVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1190, 671);
            this.Controls.Add(this.btn_play);
            this.Controls.Add(this.btn_detener);
            this.Controls.Add(this.Filtro5);
            this.Controls.Add(this.Filtro4);
            this.Controls.Add(this.Filtro3);
            this.Controls.Add(this.VFiltro2);
            this.Controls.Add(this.VFiltro01);
            this.Controls.Add(this.PB_Video);
            this.Controls.Add(this.SeleccVideo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EditarVideo";
            this.Opacity = 0.8D;
            this.Text = "EditarVideo";
            ((System.ComponentModel.ISupportInitialize)(this.PB_Video)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Filtro5;
        private System.Windows.Forms.Button Filtro4;
        private System.Windows.Forms.Button Filtro3;
        private System.Windows.Forms.Button VFiltro2;
        private System.Windows.Forms.Button VFiltro01;
        private System.Windows.Forms.PictureBox PB_Video;
        private System.Windows.Forms.Button SeleccVideo;
        private System.Windows.Forms.Button btn_detener;
        private System.Windows.Forms.Button btn_play;
    }
}