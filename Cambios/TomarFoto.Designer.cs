﻿
namespace Procesamiento_de_imagenes.Cambios
{
    partial class TomarFoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_SeleccFoto = new System.Windows.Forms.Button();
            this.PB_TFoto = new System.Windows.Forms.PictureBox();
            this.btn_TomarFoto = new System.Windows.Forms.Button();
            this.PB_ImSelecc = new System.Windows.Forms.PictureBox();
            this.CB_List = new System.Windows.Forms.ComboBox();
            this.btn_Grabar = new System.Windows.Forms.Button();
            this.btn_Detener = new System.Windows.Forms.Button();
            this.Label_Cantidad = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PB_TFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_ImSelecc)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_SeleccFoto
            // 
            this.btn_SeleccFoto.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_SeleccFoto.FlatAppearance.BorderSize = 3;
            this.btn_SeleccFoto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_SeleccFoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SeleccFoto.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SeleccFoto.ForeColor = System.Drawing.Color.White;
            this.btn_SeleccFoto.Location = new System.Drawing.Point(775, 539);
            this.btn_SeleccFoto.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_SeleccFoto.Name = "btn_SeleccFoto";
            this.btn_SeleccFoto.Size = new System.Drawing.Size(213, 49);
            this.btn_SeleccFoto.TabIndex = 11;
            this.btn_SeleccFoto.Text = "Seleccionar Fotografía";
            this.btn_SeleccFoto.UseVisualStyleBackColor = true;
            this.btn_SeleccFoto.Click += new System.EventHandler(this.btn_SeleccFoto_Click);
            // 
            // PB_TFoto
            // 
            this.PB_TFoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PB_TFoto.Location = new System.Drawing.Point(49, 154);
            this.PB_TFoto.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.PB_TFoto.Name = "PB_TFoto";
            this.PB_TFoto.Size = new System.Drawing.Size(540, 353);
            this.PB_TFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PB_TFoto.TabIndex = 10;
            this.PB_TFoto.TabStop = false;
            // 
            // btn_TomarFoto
            // 
            this.btn_TomarFoto.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_TomarFoto.FlatAppearance.BorderSize = 3;
            this.btn_TomarFoto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_TomarFoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TomarFoto.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TomarFoto.ForeColor = System.Drawing.Color.White;
            this.btn_TomarFoto.Location = new System.Drawing.Point(231, 539);
            this.btn_TomarFoto.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_TomarFoto.Name = "btn_TomarFoto";
            this.btn_TomarFoto.Size = new System.Drawing.Size(213, 49);
            this.btn_TomarFoto.TabIndex = 9;
            this.btn_TomarFoto.Text = "Tomar foto";
            this.btn_TomarFoto.UseVisualStyleBackColor = true;
            this.btn_TomarFoto.Click += new System.EventHandler(this.btn_TomarFoto_Click);
            // 
            // PB_ImSelecc
            // 
            this.PB_ImSelecc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PB_ImSelecc.Location = new System.Drawing.Point(612, 154);
            this.PB_ImSelecc.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.PB_ImSelecc.Name = "PB_ImSelecc";
            this.PB_ImSelecc.Size = new System.Drawing.Size(540, 353);
            this.PB_ImSelecc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PB_ImSelecc.TabIndex = 12;
            this.PB_ImSelecc.TabStop = false;
            // 
            // CB_List
            // 
            this.CB_List.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.CB_List.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_List.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CB_List.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.CB_List.ForeColor = System.Drawing.SystemColors.Window;
            this.CB_List.FormattingEnabled = true;
            this.CB_List.ImeMode = System.Windows.Forms.ImeMode.Katakana;
            this.CB_List.Location = new System.Drawing.Point(173, 69);
            this.CB_List.Name = "CB_List";
            this.CB_List.Size = new System.Drawing.Size(312, 30);
            this.CB_List.TabIndex = 13;
            // 
            // btn_Grabar
            // 
            this.btn_Grabar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_Grabar.FlatAppearance.BorderSize = 3;
            this.btn_Grabar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_Grabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Grabar.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Grabar.ForeColor = System.Drawing.Color.White;
            this.btn_Grabar.Location = new System.Drawing.Point(538, 59);
            this.btn_Grabar.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_Grabar.Name = "btn_Grabar";
            this.btn_Grabar.Size = new System.Drawing.Size(213, 49);
            this.btn_Grabar.TabIndex = 14;
            this.btn_Grabar.Text = "Seleccionar Cámara";
            this.btn_Grabar.UseVisualStyleBackColor = true;
            this.btn_Grabar.Click += new System.EventHandler(this.btn_Grabar_Click);
            // 
            // btn_Detener
            // 
            this.btn_Detener.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_Detener.FlatAppearance.BorderSize = 3;
            this.btn_Detener.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_Detener.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Detener.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Detener.ForeColor = System.Drawing.Color.White;
            this.btn_Detener.Location = new System.Drawing.Point(775, 59);
            this.btn_Detener.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_Detener.Name = "btn_Detener";
            this.btn_Detener.Size = new System.Drawing.Size(213, 49);
            this.btn_Detener.TabIndex = 15;
            this.btn_Detener.Text = "Detener Cámara";
            this.btn_Detener.UseVisualStyleBackColor = true;
            this.btn_Detener.Click += new System.EventHandler(this.btn_Detener_Click);
            // 
            // Label_Cantidad
            // 
            this.Label_Cantidad.AutoSize = true;
            this.Label_Cantidad.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label_Cantidad.CausesValidation = false;
            this.Label_Cantidad.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Cantidad.ForeColor = System.Drawing.Color.White;
            this.Label_Cantidad.Location = new System.Drawing.Point(731, 615);
            this.Label_Cantidad.Name = "Label_Cantidad";
            this.Label_Cantidad.Size = new System.Drawing.Size(237, 22);
            this.Label_Cantidad.TabIndex = 17;
            this.Label_Cantidad.Text = "Rostros detectados en la foto:";
            // 
            // TomarFoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1190, 671);
            this.Controls.Add(this.Label_Cantidad);
            this.Controls.Add(this.btn_Detener);
            this.Controls.Add(this.btn_Grabar);
            this.Controls.Add(this.CB_List);
            this.Controls.Add(this.PB_ImSelecc);
            this.Controls.Add(this.btn_SeleccFoto);
            this.Controls.Add(this.PB_TFoto);
            this.Controls.Add(this.btn_TomarFoto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TomarFoto";
            this.Opacity = 0.8D;
            this.Text = "TomarFoto";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TomarFoto_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.PB_TFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_ImSelecc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_SeleccFoto;
        private System.Windows.Forms.PictureBox PB_TFoto;
        private System.Windows.Forms.Button btn_TomarFoto;
        private System.Windows.Forms.PictureBox PB_ImSelecc;
        private System.Windows.Forms.ComboBox CB_List;
        private System.Windows.Forms.Button btn_Grabar;
        private System.Windows.Forms.Button btn_Detener;
        private System.Windows.Forms.Label Label_Cantidad;
    }
}