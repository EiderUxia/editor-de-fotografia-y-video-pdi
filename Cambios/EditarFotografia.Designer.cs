﻿
namespace Procesamiento_de_imagenes.Cambios
{
    partial class EditarFotografia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SeleccImagen = new System.Windows.Forms.Button();
            this.OF_SeleccImagen = new System.Windows.Forms.OpenFileDialog();
            this.Laplaciano = new System.Windows.Forms.Button();
            this.Prewitt = new System.Windows.Forms.Button();
            this.btn_Adicion = new System.Windows.Forms.Button();
            this.btn_and = new System.Windows.Forms.Button();
            this.btn_OR = new System.Windows.Forms.Button();
            this.Guardar = new System.Windows.Forms.Button();
            this.SFD_GuardarImagen = new System.Windows.Forms.SaveFileDialog();
            this.Original = new System.Windows.Forms.Button();
            this.Histograma03 = new System.Windows.Forms.PictureBox();
            this.Histograma02 = new System.Windows.Forms.PictureBox();
            this.Histograma01 = new System.Windows.Forms.PictureBox();
            this.PB_ImEdit = new System.Windows.Forms.PictureBox();
            this.btn_Grises = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Histograma03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Histograma02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Histograma01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_ImEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // SeleccImagen
            // 
            this.SeleccImagen.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.SeleccImagen.FlatAppearance.BorderSize = 3;
            this.SeleccImagen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.SeleccImagen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SeleccImagen.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SeleccImagen.ForeColor = System.Drawing.Color.White;
            this.SeleccImagen.Location = new System.Drawing.Point(195, 58);
            this.SeleccImagen.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.SeleccImagen.Name = "SeleccImagen";
            this.SeleccImagen.Size = new System.Drawing.Size(213, 49);
            this.SeleccImagen.TabIndex = 1;
            this.SeleccImagen.Text = "Seleccionar Imagen";
            this.SeleccImagen.UseVisualStyleBackColor = true;
            this.SeleccImagen.Click += new System.EventHandler(this.SeleccImagen_Click);
            // 
            // OF_SeleccImagen
            // 
            this.OF_SeleccImagen.Title = "Seleccionar Imagen";
            // 
            // Laplaciano
            // 
            this.Laplaciano.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Laplaciano.FlatAppearance.BorderSize = 3;
            this.Laplaciano.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Laplaciano.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Laplaciano.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Laplaciano.ForeColor = System.Drawing.Color.White;
            this.Laplaciano.Location = new System.Drawing.Point(617, 91);
            this.Laplaciano.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.Laplaciano.Name = "Laplaciano";
            this.Laplaciano.Size = new System.Drawing.Size(213, 49);
            this.Laplaciano.TabIndex = 3;
            this.Laplaciano.Text = "Laplaciano";
            this.Laplaciano.UseVisualStyleBackColor = true;
            this.Laplaciano.Click += new System.EventHandler(this.Laplaciano_Click);
            // 
            // Prewitt
            // 
            this.Prewitt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Prewitt.FlatAppearance.BorderSize = 3;
            this.Prewitt.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Prewitt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Prewitt.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prewitt.ForeColor = System.Drawing.Color.White;
            this.Prewitt.Location = new System.Drawing.Point(617, 181);
            this.Prewitt.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.Prewitt.Name = "Prewitt";
            this.Prewitt.Size = new System.Drawing.Size(213, 49);
            this.Prewitt.TabIndex = 4;
            this.Prewitt.Text = "Prewitt";
            this.Prewitt.UseVisualStyleBackColor = true;
            this.Prewitt.Click += new System.EventHandler(this.Prewitt_Click);
            // 
            // btn_Adicion
            // 
            this.btn_Adicion.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_Adicion.FlatAppearance.BorderSize = 3;
            this.btn_Adicion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_Adicion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Adicion.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Adicion.ForeColor = System.Drawing.Color.White;
            this.btn_Adicion.Location = new System.Drawing.Point(617, 271);
            this.btn_Adicion.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_Adicion.Name = "btn_Adicion";
            this.btn_Adicion.Size = new System.Drawing.Size(213, 49);
            this.btn_Adicion.TabIndex = 5;
            this.btn_Adicion.Text = "Adicion";
            this.btn_Adicion.UseVisualStyleBackColor = true;
            this.btn_Adicion.Click += new System.EventHandler(this.btn_Adicion_Click);
            // 
            // btn_and
            // 
            this.btn_and.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_and.FlatAppearance.BorderSize = 3;
            this.btn_and.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_and.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_and.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_and.ForeColor = System.Drawing.Color.White;
            this.btn_and.Location = new System.Drawing.Point(617, 361);
            this.btn_and.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_and.Name = "btn_and";
            this.btn_and.Size = new System.Drawing.Size(213, 49);
            this.btn_and.TabIndex = 6;
            this.btn_and.Text = "AND";
            this.btn_and.UseVisualStyleBackColor = true;
            this.btn_and.Click += new System.EventHandler(this.btn_and_Click);
            // 
            // btn_OR
            // 
            this.btn_OR.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_OR.FlatAppearance.BorderSize = 3;
            this.btn_OR.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_OR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_OR.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OR.ForeColor = System.Drawing.Color.White;
            this.btn_OR.Location = new System.Drawing.Point(617, 451);
            this.btn_OR.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_OR.Name = "btn_OR";
            this.btn_OR.Size = new System.Drawing.Size(213, 49);
            this.btn_OR.TabIndex = 7;
            this.btn_OR.Text = "OR";
            this.btn_OR.UseVisualStyleBackColor = true;
            this.btn_OR.Click += new System.EventHandler(this.btn_OR_Click);
            // 
            // Guardar
            // 
            this.Guardar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Guardar.FlatAppearance.BorderSize = 3;
            this.Guardar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Guardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Guardar.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guardar.ForeColor = System.Drawing.Color.White;
            this.Guardar.Location = new System.Drawing.Point(195, 536);
            this.Guardar.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(213, 49);
            this.Guardar.TabIndex = 8;
            this.Guardar.Text = "Guardar Imagen";
            this.Guardar.UseVisualStyleBackColor = true;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // Original
            // 
            this.Original.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Original.FlatAppearance.BorderSize = 3;
            this.Original.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.Original.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Original.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Original.ForeColor = System.Drawing.Color.White;
            this.Original.Location = new System.Drawing.Point(897, 598);
            this.Original.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.Original.Name = "Original";
            this.Original.Size = new System.Drawing.Size(213, 49);
            this.Original.TabIndex = 9;
            this.Original.Text = "Foto original";
            this.Original.UseVisualStyleBackColor = true;
            this.Original.Click += new System.EventHandler(this.Original_Click);
            // 
            // Histograma03
            // 
            this.Histograma03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Histograma03.Location = new System.Drawing.Point(863, 414);
            this.Histograma03.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.Histograma03.Name = "Histograma03";
            this.Histograma03.Size = new System.Drawing.Size(280, 171);
            this.Histograma03.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Histograma03.TabIndex = 12;
            this.Histograma03.TabStop = false;
            this.Histograma03.Paint += new System.Windows.Forms.PaintEventHandler(this.Histograma03_Paint);
            // 
            // Histograma02
            // 
            this.Histograma02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Histograma02.Location = new System.Drawing.Point(863, 220);
            this.Histograma02.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.Histograma02.Name = "Histograma02";
            this.Histograma02.Size = new System.Drawing.Size(280, 171);
            this.Histograma02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Histograma02.TabIndex = 11;
            this.Histograma02.TabStop = false;
            this.Histograma02.Paint += new System.Windows.Forms.PaintEventHandler(this.Histograma02_Paint);
            // 
            // Histograma01
            // 
            this.Histograma01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Histograma01.Location = new System.Drawing.Point(863, 26);
            this.Histograma01.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.Histograma01.Name = "Histograma01";
            this.Histograma01.Size = new System.Drawing.Size(280, 171);
            this.Histograma01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Histograma01.TabIndex = 10;
            this.Histograma01.TabStop = false;
            this.Histograma01.Paint += new System.Windows.Forms.PaintEventHandler(this.Histograma01_Paint_1);
            // 
            // PB_ImEdit
            // 
            this.PB_ImEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PB_ImEdit.Location = new System.Drawing.Point(41, 130);
            this.PB_ImEdit.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.PB_ImEdit.Name = "PB_ImEdit";
            this.PB_ImEdit.Size = new System.Drawing.Size(540, 353);
            this.PB_ImEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PB_ImEdit.TabIndex = 2;
            this.PB_ImEdit.TabStop = false;
            // 
            // btn_Grises
            // 
            this.btn_Grises.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_Grises.FlatAppearance.BorderSize = 3;
            this.btn_Grises.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(135)))), ((int)(((byte)(198)))));
            this.btn_Grises.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Grises.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Grises.ForeColor = System.Drawing.Color.White;
            this.btn_Grises.Location = new System.Drawing.Point(617, 598);
            this.btn_Grises.Margin = new System.Windows.Forms.Padding(50, 50, 3, 3);
            this.btn_Grises.Name = "btn_Grises";
            this.btn_Grises.Size = new System.Drawing.Size(213, 49);
            this.btn_Grises.TabIndex = 14;
            this.btn_Grises.Text = "Escala de grises";
            this.btn_Grises.UseVisualStyleBackColor = true;
            this.btn_Grises.Click += new System.EventHandler(this.btn_Grises_Click);
            // 
            // EditarFotografia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1190, 671);
            this.Controls.Add(this.btn_Grises);
            this.Controls.Add(this.Histograma03);
            this.Controls.Add(this.Histograma02);
            this.Controls.Add(this.Histograma01);
            this.Controls.Add(this.Original);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.btn_OR);
            this.Controls.Add(this.btn_and);
            this.Controls.Add(this.btn_Adicion);
            this.Controls.Add(this.Prewitt);
            this.Controls.Add(this.Laplaciano);
            this.Controls.Add(this.PB_ImEdit);
            this.Controls.Add(this.SeleccImagen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EditarFotografia";
            this.Opacity = 0.8D;
            this.Text = "EditarFotografia";
            ((System.ComponentModel.ISupportInitialize)(this.Histograma03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Histograma02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Histograma01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_ImEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SeleccImagen;
        private System.Windows.Forms.PictureBox PB_ImEdit;
        private System.Windows.Forms.OpenFileDialog OF_SeleccImagen;
        private System.Windows.Forms.Button Laplaciano;
        private System.Windows.Forms.Button Prewitt;
        private System.Windows.Forms.Button btn_Adicion;
        private System.Windows.Forms.Button btn_and;
        private System.Windows.Forms.Button btn_OR;
        private System.Windows.Forms.Button Guardar;
        private System.Windows.Forms.SaveFileDialog SFD_GuardarImagen;
        private System.Windows.Forms.Button Original;
        private System.Windows.Forms.PictureBox Histograma01;
        private System.Windows.Forms.PictureBox Histograma02;
        private System.Windows.Forms.PictureBox Histograma03;
        private System.Windows.Forms.Button btn_Grises;
    }
}