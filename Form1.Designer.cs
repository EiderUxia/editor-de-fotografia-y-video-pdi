﻿
namespace Procesamiento_de_imagenes
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelControl = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnMinimizar = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            this.panelBotones = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnEditVid = new System.Windows.Forms.Button();
            this.btnEditFoto = new System.Windows.Forms.Button();
            this.btnTomFot = new System.Windows.Forms.Button();
            this.panelCambio = new System.Windows.Forms.Panel();
            this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
            this.panelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            this.panelBotones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelCambio.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl
            // 
            this.panelControl.Controls.Add(this.pictureBox2);
            this.panelControl.Controls.Add(this.btnMinimizar);
            this.panelControl.Controls.Add(this.btnCerrar);
            this.panelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl.Location = new System.Drawing.Point(0, 0);
            this.panelControl.Name = "panelControl";
            this.panelControl.Size = new System.Drawing.Size(1404, 55);
            this.panelControl.TabIndex = 0;
            this.panelControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelControl_MouseMove);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(95, 55);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnMinimizar.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.Image")));
            this.btnMinimizar.Location = new System.Drawing.Point(1269, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(67, 55);
            this.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimizar.TabIndex = 1;
            this.btnMinimizar.TabStop = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.Location = new System.Drawing.Point(1336, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(68, 55);
            this.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnCerrar.TabIndex = 0;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // panelBotones
            // 
            this.panelBotones.Controls.Add(this.pictureBox1);
            this.panelBotones.Controls.Add(this.btnEditVid);
            this.panelBotones.Controls.Add(this.btnEditFoto);
            this.panelBotones.Controls.Add(this.btnTomFot);
            this.panelBotones.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelBotones.Location = new System.Drawing.Point(0, 55);
            this.panelBotones.Name = "panelBotones";
            this.panelBotones.Size = new System.Drawing.Size(214, 671);
            this.panelBotones.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(189, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 612);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnEditVid
            // 
            this.btnEditVid.FlatAppearance.BorderSize = 0;
            this.btnEditVid.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(16)))), ((int)(((byte)(142)))));
            this.btnEditVid.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(16)))), ((int)(((byte)(181)))));
            this.btnEditVid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditVid.Font = new System.Drawing.Font("Open Sans Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditVid.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnEditVid.Location = new System.Drawing.Point(0, 344);
            this.btnEditVid.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.btnEditVid.Name = "btnEditVid";
            this.btnEditVid.Size = new System.Drawing.Size(196, 86);
            this.btnEditVid.TabIndex = 2;
            this.btnEditVid.Text = "Editar video";
            this.btnEditVid.UseVisualStyleBackColor = true;
            this.btnEditVid.Click += new System.EventHandler(this.btnEditVid_Click);
            // 
            // btnEditFoto
            // 
            this.btnEditFoto.FlatAppearance.BorderSize = 0;
            this.btnEditFoto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(16)))), ((int)(((byte)(142)))));
            this.btnEditFoto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(16)))), ((int)(((byte)(181)))));
            this.btnEditFoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditFoto.Font = new System.Drawing.Font("Open Sans Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditFoto.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnEditFoto.Location = new System.Drawing.Point(0, 235);
            this.btnEditFoto.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.btnEditFoto.Name = "btnEditFoto";
            this.btnEditFoto.Size = new System.Drawing.Size(196, 86);
            this.btnEditFoto.TabIndex = 1;
            this.btnEditFoto.Text = "Editar fotografía";
            this.btnEditFoto.UseVisualStyleBackColor = true;
            this.btnEditFoto.Click += new System.EventHandler(this.btnEditFoto_Click);
            // 
            // btnTomFot
            // 
            this.btnTomFot.FlatAppearance.BorderSize = 0;
            this.btnTomFot.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(16)))), ((int)(((byte)(142)))));
            this.btnTomFot.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(16)))), ((int)(((byte)(181)))));
            this.btnTomFot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTomFot.Font = new System.Drawing.Font("Open Sans Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTomFot.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnTomFot.Location = new System.Drawing.Point(0, 126);
            this.btnTomFot.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.btnTomFot.Name = "btnTomFot";
            this.btnTomFot.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnTomFot.Size = new System.Drawing.Size(196, 86);
            this.btnTomFot.TabIndex = 0;
            this.btnTomFot.Text = "Tomar fotografía";
            this.btnTomFot.UseVisualStyleBackColor = true;
            this.btnTomFot.Click += new System.EventHandler(this.btnTomFot_Click);
            // 
            // panelCambio
            // 
            this.panelCambio.Controls.Add(this.domainUpDown1);
            this.panelCambio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCambio.Location = new System.Drawing.Point(214, 55);
            this.panelCambio.Name = "panelCambio";
            this.panelCambio.Size = new System.Drawing.Size(1190, 671);
            this.panelCambio.TabIndex = 2;
            // 
            // domainUpDown1
            // 
            this.domainUpDown1.Location = new System.Drawing.Point(62, 364);
            this.domainUpDown1.Name = "domainUpDown1";
            this.domainUpDown1.Size = new System.Drawing.Size(120, 20);
            this.domainUpDown1.TabIndex = 0;
            this.domainUpDown1.Text = "domainUpDown1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1404, 726);
            this.Controls.Add(this.panelCambio);
            this.Controls.Add(this.panelBotones);
            this.Controls.Add(this.panelControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            this.panelBotones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelCambio.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelControl;
        private System.Windows.Forms.Panel panelBotones;
        private System.Windows.Forms.Panel panelCambio;
        private System.Windows.Forms.PictureBox btnMinimizar;
        private System.Windows.Forms.PictureBox btnCerrar;
        private System.Windows.Forms.Button btnTomFot;
        private System.Windows.Forms.Button btnEditVid;
        private System.Windows.Forms.Button btnEditFoto;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.DomainUpDown domainUpDown1;
    }
}

